#!/usr/bin/env python
# -*- coding:utf-8 vi:noet
# Tape ass-istant
# SPDX-FileCopyrightText: 2017,2023 Jérôme Carretero <cJ-pulsemeter@zougloub.eu>
# SPDX-License-Identifier: GPL-3.0-only

import sys
import errno
import io
import os
import queue
import logging
import time
import threading
import contextlib
import mmap

from humanfriendly import (
 format_size,
 format_timespan,
)


logger = logging.getLogger(__name__)


def main():
	import argparse

	logger = logging.getLogger()

	parser = argparse.ArgumentParser(
	 description="k7 - tape writing assistant",
	)

	log_level = os.environ.get("K7_LOG_LEVEL", "WARNING")

	parser.add_argument("--log-level",
	 default=log_level,
	 help="Logging level (see Python logging docs); default {}".format(log_level),
	)

	subparsers = parser.add_subparsers(
	 help=f'the command; type "{sys.argv[0]} COMMAND -h" for command-specific help',
	 dest='command',
	)


	subp = subparsers.add_parser(
	 "cat",
	 help="Read from a file and write to stdout (without filling file cache)",
	)
	subp.add_argument("src",
	 help="Source file",
	 nargs="?",
	)
	subp.add_argument("--chunk-size",
	 help="Size of (read) chunk",
	 default=1<<20,
	 type=int,
	)

	def do_cat(args):
		fd = os.open(args.src, os.O_RDONLY)
		size = os.lseek(fd, 0, os.SEEK_END)
		os.lseek(fd, 0, os.SEEK_SET)
		logger.debug("Opened %d=%s size %d chunksize=%d",
		 fd, args.src, size, args.chunk_size)
		MADV_PAGEOUT = 21
		advice = MADV_PAGEOUT
		m = mmap.mmap(fd, size, prot=mmap.PROT_READ, flags=mmap.MAP_PRIVATE)
		m.madvise(mmap.MADV_SEQUENTIAL)
		done = 0
		todo = size
		while done < size:
			chunksize = min(args.chunk_size, size-done)
			chunk = m.read(chunksize)
			m.madvise(advice, done, args.chunk_size)
			sys.stdout.buffer.write(chunk)
			done += chunksize

		os.close(fd)

	subp.set_defaults(func=do_cat)


	subp = subparsers.add_parser(
	 "i",
	 help="Input from file",
	)
	subp.add_argument("src",
	 help="Source file",
	 nargs="?",
	)

	def do_i(args):
		raise NotImplementedError()

	subp.set_defaults(func=do_i)


	subp = subparsers.add_parser(
	 "o",
	 help="Read from stdin and write to (tape device) file",
	)
	subp.add_argument("dst",
	 help="Target file",
	 nargs="?",
	)
	subp.add_argument("--chunk-size",
	 help="Size of (written) chunk",
	 default=512<<10,
	 type=int,
	)
	subp.add_argument("--buffer-size",
	 help="Size of in-memory buffer (will be rounded to a multiple of chunk size)",
	 default=1<<30,
	 type=int,
	)

	def do_o(args):
		"""
		"""
		buffer_chunks = args.buffer_size // args.chunk_size
		q = queue.Queue(maxsize=buffer_chunks)

		running = True

		fd = os.open(args.dst, os.O_CREAT|os.O_WRONLY)#|os.O_DIRECT)

		working = [1, 1]

		processed = [0,0]

		t_beg = time.monotonic()
		busy = [0,0]
		n_enospc = [0]

		def r_work():
			try:
				while running:
					t0 = time.monotonic()
					chunk = sys.stdin.buffer.read(args.chunk_size)
					t1 = time.monotonic()
					busy[0] += (t1 - t0)
					if not chunk:
						break
					logger.debug("Put chunk")
					processed[0] += len(chunk)
					q.put(chunk, block=True)
				q.put(None, block=True)
				working[0] = 0
			except Exception as e:
				logger.exception("R: unexpected: %s", e)

		def w_work():
			try:
				while running:
					while running and working[0] and q.qsize() * args.chunk_size < args.buffer_size * 99 // 100:
						logger.debug("Waiting for queue to be full (%f %%)",
						 q.qsize() * args.chunk_size * 100 / args.buffer_size)
						time.sleep(1)
					logger.debug("Start emptying")
					while not q.empty():
						if not running:
							return
						logger.debug("Get chunk, did %s", processed[1])
						chunk = q.get()
						if chunk is None:
							working[1] = 0
							return
						t0 = time.monotonic()
						for idx_attempt in range(2):
							try:
								os.write(fd, chunk)
								if idx_attempt == 1:
									n_enospc[0] += 1
								break
							except OSError as e:
								if e.errno == errno.ENOSPC and idx_attempt == 0:
									continue
								working[1] = 0
								raise
						t1 = time.monotonic()
						busy[1] += (t1 - t0)
						processed[1] += len(chunk)
			except Exception as e:
				logger.exception("W: unexpected: %s", e)

		t_w = threading.Thread(target=w_work)
		t_r = threading.Thread(target=r_work)

		try:
			t_w.start()
			t_r.start()

			busy_ = [0,0]
			processed_ = [0,0]
			while sum(working) > 0:
				t_now = time.monotonic()
				logger.info("B=%7.3f%% S=% 9s/s S_R=% 9s/s (% 9s/s) S_W=% 9s/s (% 9s/s) R=%dB W=%dB EW=%d in %s",
				 q.qsize() * args.chunk_size * 100 / args.buffer_size,
				 format_size(processed[1] / (0.0001 + t_now-t_beg)),
				 format_size((processed[0] - processed_[0]) / (0.0001 + busy[0] - busy_[0])),
				 format_size(processed[0] / (0.0001 + busy[0])),
				 format_size((processed[1] - processed_[1]) / (0.0001 + busy[1] - busy_[1])),
				 format_size(processed[1] / (0.0001 + busy[1])),
				 processed[0], processed[1],
				 n_enospc[0],
				 format_timespan(int(round(t_now-t_beg))),
				)
				processed_[:] = processed[0], processed[1]
				busy_[:] = busy[0], busy[1]

				time.sleep(1)
		except KeyboardInterrupt:
			logger.warning("Interrupted")
			running = False
			q.put(None)
		except Exception as e:
			logger.exception("Unexpected: %s", e)
			raise
		finally:
			t_r.join()
			t_w.join()

		t_end = time.monotonic()

		logger.info("S=% 9s/s S_R=% 9s/s S_W=% 9s/s R=%dB W=%dB in %s",
		 format_size(processed[1] / (0.0001 + t_end-t_beg)),
		 format_size(processed[0] / (0.0001 + busy[0])),
		 format_size(processed[1] / (0.0001 + busy[1])),
		 processed[0], processed[1],
		 format_timespan(t_end-t_beg),
		)

		if n_enospc[0]:
			# Early Warning End Of Media notification
			logger.info("EWEOM count: %d", n_enospc[0])

		os.close(fd)

	subp.set_defaults(func=do_o)


	with contextlib.suppress(BaseException):
		import argcomplete
		argcomplete.autocomplete(parser)

	args = parser.parse_args()

	try:
		import coloredlogs
		coloredlogs.install(
		 level=getattr(logging, args.log_level),
		 logger=logger,
		)
	except ImportError:
		logging.basicConfig(
		 datefmt="%Y%m%dT%H%M%S",
		 level=getattr(logging, args.log_level),
		 format="%(asctime)-15s %(name)s %(levelname)s %(message)s"
		)

	if getattr(args, 'func', None) is None:
		parser.print_help()
		return 1

	return args.func(args)


if __name__ == '__main__':
	res = main()
	raise SystemExit(res)
